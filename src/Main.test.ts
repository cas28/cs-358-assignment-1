import "jest-extended";

import {
  List, ConsCell,
  cons, snoc, doubleEachIterative, doubleEachRecursive, reverse, removeNegatives,
  countLeaves, removeNots
} from "./Main";

const testList1: List = null;
const testList2: List = { first: 0, rest: testList1 };
const testList3: List = { first: -1, rest: { first: 1, rest: testList2 } };
const testList4: List = { first: 1000, rest: { first: -1000, rest: testList2 } };

test("doubleEachIterative test", () => {
  expect(doubleEachIterative(null)).toEqual(null);

  expect(doubleEachIterative({
    first: 0,
    rest: null
  })).toEqual({
    first: 0,
    rest: null
  });

  expect(doubleEachIterative({
    first: 1,
    rest: null
  })).toEqual({
    first: 2,
    rest: null
  });

  expect(doubleEachIterative({
    first: -1,
    rest: {
      first: 1,
      rest: {
        first: 0,
        rest: null
      }
    }
  })).toEqual({
    first: -2,
    rest: {
      first: 2,
      rest: {
        first: 0,
        rest: null
      }
    }
  });

  expect(doubleEachIterative({
    first: -100,
    rest: {
      first: 100,
      rest: {
        first: 1,
        rest: {
          first: 2,
          rest: null
        }
      }
    }
  })).toEqual({
    first: -200,
    rest: {
      first: 200,
      rest: {
        first: 2,
        rest: {
          first: 4,
          rest: null
        }
      }
    }
  });
});

test("doubleEachRecursive test", () => {
  expect(doubleEachRecursive(null)).toEqual(null);

  expect(doubleEachRecursive({
    first: 0,
    rest: null
  })).toEqual({
    first: 0,
    rest: null
  });

  expect(doubleEachRecursive({
    first: 1,
    rest: null
  })).toEqual({
    first: 2,
    rest: null
  });

  expect(doubleEachRecursive({
    first: -1,
    rest: {
      first: 1,
      rest: {
        first: 0,
        rest: null
      }
    }
  })).toEqual({
    first: -2,
    rest: {
      first: 2,
      rest: {
        first: 0,
        rest: null
      }
    }
  });

  expect(doubleEachRecursive({
    first: -100,
    rest: {
      first: 100,
      rest: {
        first: 1,
        rest: {
          first: 2,
          rest: null
        }
      }
    }
  })).toEqual({
    first: -200,
    rest: {
      first: 200,
      rest: {
        first: 2,
        rest: {
          first: 4,
          rest: null
        }
      }
    }
  });
});

test("reverse test", () => {
  expect(reverse(null)).toEqual(null);

  expect(reverse({
    first: 0,
    rest: null
  })).toEqual({
    first: 0,
    rest: null
  });

  expect(reverse({
    first: 1,
    rest: null
  })).toEqual({
    first: 1,
    rest: null
  });

  expect(reverse({
    first: -1,
    rest: {
      first: 1,
      rest: {
        first: 0,
        rest: null
      }
    }
  })).toEqual({
    first: 0,
    rest: {
      first: 1,
      rest: {
        first: -1,
        rest: null
      }
    }
  });

  expect(reverse({
    first: -100,
    rest: {
      first: 100,
      rest: {
        first: 1,
        rest: {
          first: 2,
          rest: null
        }
      }
    }
  })).toEqual({
    first: 2,
    rest: {
      first: 1,
      rest: {
        first: 100,
        rest: {
          first: -100,
          rest: null
        }
      }
    }
  });
});


test("removeNegatives test", () => {
  expect(removeNegatives(null)).toEqual(null);

  expect(removeNegatives({
    first: 0,
    rest: null
  })).toEqual({
    first: 0,
    rest: null
  });

  expect(removeNegatives({
    first: 1,
    rest: null
  })).toEqual({
    first: 1,
    rest: null
  });

  expect(removeNegatives({
    first: -1,
    rest: null
  })).toEqual(
    null
  );

  expect(removeNegatives({
    first: -1,
    rest: {
      first: 1,
      rest: {
        first: 0,
        rest: null
      }
    }
  })).toEqual({
    first: 1,
    rest: {
      first: 0,
      rest: null
    }
  });

  expect(removeNegatives({
    first: -100,
    rest: {
      first: 100,
      rest: {
        first: 1,
        rest: {
          first: -2,
          rest: null
        }
      }
    }
  })).toEqual({
    first: 100,
    rest: {
      first: 1,
      rest: null
    }
  });
});

test("countLeaves test", () => {
  expect(countLeaves({ tag: "bool", value: true })).toEqual(1);

  expect(countLeaves({ tag: "bool", value: false })).toEqual(1);

  expect(countLeaves({
    tag: "not",
    subtree: { tag: "bool", value: true }
  })).toEqual(1);

  expect(countLeaves({
    tag: "not",
    subtree: { tag: "bool", value: false }
  })).toEqual(1);

  expect(countLeaves({
    tag: "not",
    subtree: {
      tag: "not",
      subtree: { tag: "bool", value: true }
    }
  })).toEqual(1);

  expect(countLeaves({
    tag: "or",
    leftSubtree: { tag: "bool", value: true },
    rightSubtree: { tag: "bool", value: false }
  })).toEqual(2);

  expect(countLeaves({
    tag: "or",
    leftSubtree: {
      tag: "not",
      subtree: { tag: "bool", value: true }
    },
    rightSubtree: { tag: "bool", value: false }
  })).toEqual(2);

  expect(countLeaves({
    tag: "or",
    leftSubtree: {
      tag: "or",
      leftSubtree: { tag: "bool", value: true },
      rightSubtree: { tag: "bool", value: true }
    },
    rightSubtree: {
      tag: "not",
      subtree: { tag: "bool", value: false }
    }
  })).toEqual(3);
});

test("removeNots test", () => {
  expect(removeNots({ tag: "bool", value: true })).toEqual({ tag: "bool", value: true});

  expect(removeNots({ tag: "bool", value: false })).toEqual({ tag: "bool", value: false});

  expect(removeNots({
    tag: "not",
    subtree: { tag: "bool", value: true }
  })).toEqual({
    tag: "bool", value: true
  });

  expect(removeNots({
    tag: "not",
    subtree: { tag: "bool", value: false }
  })).toEqual({
    tag: "bool", value: false
  });

  expect(removeNots({
    tag: "not",
    subtree: {
      tag: "not",
      subtree: { tag: "bool", value: true }
    }
  })).toEqual({
    tag: "bool", value: true
  });

  expect(removeNots({
    tag: "or",
    leftSubtree: {
      tag: "not",
      subtree: { tag: "bool", value: true }
    },
    rightSubtree: {
      tag: "not",
      subtree: { tag: "bool", value: false }
    }
  })).toEqual({
    tag: "or",
    leftSubtree: { tag: "bool", value: true },
    rightSubtree: { tag: "bool", value: false }
  });

  expect(removeNots({
    tag: "not",
    subtree: {
      tag: "or",
      leftSubtree: { tag: "bool", value: true },
      rightSubtree: { tag: "bool", value: false },
    }
  })).toEqual({
    tag: "or",
    leftSubtree: { tag: "bool", value: true },
    rightSubtree: { tag: "bool", value: false }
  });

  expect(removeNots({
    tag: "not",
    subtree: {
      tag: "or",
      leftSubtree: {
        tag: "not",
        subtree: {
          tag: "not",
          subtree: {
            tag: "not",
            subtree: { tag: "bool", value: true}
          }
        }
      },
      rightSubtree: {
        tag: "not",
        subtree: {
          tag: "or",
          leftSubtree: { tag: "bool", value: false },
          rightSubtree: { tag: "bool", value: true }
        }
      },
    }
  })).toEqual({
    tag: "or",
    leftSubtree: { tag: "bool", value: true },
    rightSubtree: {
      tag: "or",
      leftSubtree: { tag: "bool", value: false },
      rightSubtree: { tag: "bool", value: true }
    }
  });
});